/**
 * @author Daniel
 * @author Andrew
 * Created 3/29/2016
 * 
 * YahtzeeMain class initialized players and a game, and runs them.
 */

package view;

import java.util.ArrayList;
import model.*;

public class YahtzeeMain {

	public static void main(String[] args) {
		final int NUM_GAMES = 5;
		int numPlayers = 0;
		int runningScore = 0;
		int[] gameScores = new int[NUM_GAMES];
		for (int i = 0; i < NUM_GAMES; i++) {

			ArrayList<Player> players = new ArrayList<Player>();
			players.add(new Player("Andrew/Daniel", false));
			
			numPlayers = players.size();

			Game game = new Game.Builder()
					.players(players)
					.build();

			game.beginGame();

			for (Player p : game.getPlayers()) {
				runningScore += p.getCurrentScore();
				gameScores[i] = p.getCurrentScore();
			}
		}
		System.out.println();
		
		for(int i =0; i < gameScores.length; i++)
			System.out.println(gameScores[i]);
		
		System.out.println("***   Average score: " + ((float) runningScore / (float) (NUM_GAMES * numPlayers)) + "    ***");

	}

}
