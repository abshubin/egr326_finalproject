/**
 * @author Daniel
 * Created 3/31/2016
 * 
 * FourAndUpStrategy class implements YahtzeeStrategy to keep dice rolled 4 or higher
 */

package model;

import java.util.HashSet;
import java.util.Set;

public class FourAndUpStrategy  implements YahtzeeStrategy {

	@Override
	public void reRollDice(Dice dice, ScoreCard sc) {
		Set<Integer> indices = new HashSet<>();
		
		for(int i = 0; i < dice.getDice().size(); i++){
			if(dice.getDice().get(i).getLastRoll() < 4){
				indices.add(i);
			}
		}
		dice.rollSpecifiedDie(indices);
		
		System.out.println("FOUR AND UP STRATEGY SELECTED");
		System.out.println("Re-rolling dice less than 4. " + dice);
		
	}

	@Override
	public void selectCategory(Dice dice, ScoreCard sc) {
		
		System.out.println("FOUR AND UP STRATEGY SELECTED");
		System.out.println("Selecting highest scoring category: " + sc.getHighestScoringCategory(dice));
		System.out.println("Score: " + sc.getHighestScoringCategory(dice).getScore(dice));
		
		sc.useCategory(sc.getHighestScoringCategory(dice), dice);
		
	}

}