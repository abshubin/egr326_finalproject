/**
 * @author Andrew
 * Created 4/5/2016
 * 
 * HumanPlayerStrategy class implements YahtzeeStrategy to let a human make the choices
 */

package model;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

import model.ScoringCategories.*;

public class HumanPlayerStrategy implements YahtzeeStrategy {

	@Override
	public void reRollDice(Dice dice, ScoreCard sc) {
		char opt;
		
		out();
		out("Do you wish to re-roll any? (y/n)");
		opt = in();
		switch (opt) {
			case 'y':
				chooseDice(dice); // chooses dice to re-roll, and re-rolls them
				out("HUMAN STRATEGY EXECUTED");
				out("Re-rolling dice.");
				out("New dice values: " + dice.toString());
				break;
			case 'n':
				out("Note: you may be asked this once more.");
				break;
			default:
				out("Please input either a 'y' or an 'n'...");
		}
	}

	@Override
	public void selectCategory(Dice dice, ScoreCard sc) {
		int score = 0;
		ScoreCategory chosenCat = null;
		Iterator<ScoreCategory> cats = sc.keyIterator();
		
		out();
		out("Current score card:");
		out(sc.toString());
		
		out("Pick a category to score the following dice set in: " + dice.toString());
		
		int k = 0; // used to index cats iterator
		char[] j = {'a', 'b', 'c', 'd', 'e', 'f', 'g',
						'h', 'i', 'j', 'k', 'l', 'm'}; // ui labels for categories
		ScoreCategory[] catList = new ScoreCategory[13];
		while (cats.hasNext()) { // iterate through, outputing as table with what each category
								// is worth with these dice, as well as the index from j[]...
			ScoreCategory c = cats.next();
			catList[k] = c; // save them indexed in an array for later use...
			out(j[k] + " " + c.toString() + " - worth " + c.getScore(dice) + " points");
			k++;
		}
		
		out();
		out("Please enter a selector (" + j[0] + " through " + j[k-1] + "):");
		out("(Make sure you don't choose one you've already chosen!)");
		
		char opt = in();
		for (int i = 0 ; i < k ; i++) {
			if (j[i] == opt) { // this for-if combo is to match the imput with the index from j[]...
				chosenCat = catList[i]; // ...then select the affiliated category from the list
				if (sc.getScoreSheet().get(chosenCat).isUsed()) {
					chosenCat = null;
					out("Category already chosen!");
				}
			} // this was better than doing a switch with a case for each category...
		}
		if (chosenCat == null) {
			chosenCat = sc.getHighestScoringCategory(dice);
			out("Inpropper input, defaulting to '" + chosenCat.toString() + "'...");
		}
		score = chosenCat.getScore(dice);
		
		out("HUMAN STRATEGY EXECUTED");
		out("Selecting category: " + chosenCat.toString());
		out("Score: " + score);
		
		sc.useCategory(chosenCat, dice);
	}
	
	private void chooseDice(Dice dice) {
		Set<Integer> indices = new HashSet<>();
		char opt = '_';
		out();
		out("Dice Labels: a b c d e");
		out("Dice Values: " + dice.toString());
		out("Please enter (seperately) each label of dice to be changed: ");
		out("(Enter 'f' to finish selection)");
		boolean run = true;
		while (run) {
			opt = in();
			switch (opt) {
				case 'a':
					indices.add(0);
					break;
				case 'b':
					indices.add(1);
					break;
				case 'c':
					indices.add(2);
					break;
				case 'd':
					indices.add(3);
					break;
				case 'e':
					indices.add(4);
					break;
				case 'f':
					run = false;
					break;
				default:
					break;
			}
		}
		dice.rollSpecifiedDie(indices);
	}

	private static void out(String printedLine) { // for simpler coding since
		System.out.println(printedLine); // this line will be used a lot
	}
	
	private static void out() {
		System.out.println();
	}
	
	@SuppressWarnings("resource")
	private static char in() {
		char c = '_';
		Scanner read = new Scanner(System.in);
		c = read.nextLine().charAt(0);
		return c;
	}
}
