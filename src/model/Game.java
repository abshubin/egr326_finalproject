/**
 * @author Daniel
 * @author Andrew
 * Created 3/31/2016
 * 
 * Game class implements Builder design pattern to ensure an immutable game
 * Also implements State design pattern for keeping track of the state of the game
 */

package model;

import java.util.ArrayList;

public class Game {
	public static enum GameState {FIRST_ROLL, SECOND_ROLL, THIRD_ROLL, END_TURN}; // STATE DESIGN PATTERN
	private final int NUM_ROUNDS = 13;
	private final static int NUM_DICE = 5;
	
	private final ArrayList<Player> players;
	private final Dice dice;
	
	private Game(Builder builder){
		this.players = builder.players;
		this.dice = builder.dice;
	}
	
	// ACCESSORS
	public ArrayList<Player> getPlayers() { return new ArrayList<Player>(players); }
	
	public void beginGame(){
		for (int i = 0; i < NUM_ROUNDS; i++){ // The game is 13 rounds
			System.out.println("*** ROUND " + (i + 1) + " ***"); // For console only
			
			for (Player p : players){ // Each player takes a turn each round
				System.out.println(p.getName() + "'s TURN");
				
				p.setState(GameState.FIRST_ROLL);  // Begin player's turn
				dice.rollAll(); // Automatically roll all dice for first roll
				System.out.println(dice);
				
				while (p.getState() != GameState.END_TURN){
					
					YahtzeeStrategy strat = null;
					if(p.isHuman())
						strat = new HumanPlayerStrategy();
					else
						strat = determineBestStrategy(dice, p.getScoreCard());
					p.setStrategy(strat);
					p.executeStrategy(dice);
				}
				System.out.println(p);
				System.out.println(); // Just for better readability in the console
			}
			System.out.println(); // Just for better readability in the console
		}
		
		// GAME OVER, PRINT FINAL SCORES
		Player winner = players.get(0);
		for(Player p : players){
			System.out.println(p);
			//System.out.println(p.getScoreCard());
			if(p.getCurrentScore() >= winner.getCurrentScore())
				winner = p;
		}
		System.out.println("WINNER: " + winner);
	}
	
	private YahtzeeStrategy determineBestStrategy(Dice dice, ScoreCard sc){
		
//		StrategyPicker pick = new StrategyPicker(dice, sc);
//		return pick.strategize();
		return new DanielStrategy();
//		return new AndrewStrategy();
		
//		/*** This is just dummy code for testing: ***/
//		Random rand = new Random();
//		switch(rand.nextInt(3)){ // returns number from 0 to 2
//		case 0: return new OfAKinderStrategy();
//		case 1: return new UpperSectionerStrategy();
//		case 2: return new FourAndUpStrategy();
//		default: return new RandomStrategy();  // never used
//		}
	}
	
	// PRIVATE BUILDER CLASS FOR IMMUTABILITY
	public static class Builder{
		private ArrayList<Player> players;
		private Dice dice;
		
		public Builder(){
			dice = new Dice(NUM_DICE);
		}
		
		public Builder players(ArrayList<Player> players){
			this.players = new ArrayList<Player>(players);
			return this;
		}
		
		public Game build(){
			return new Game(this);
		}
	}
	
}
