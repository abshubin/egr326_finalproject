/**
 * @author Andrew
 * Created 4/8/2016
 * 
 * StrategyPicker takes the given dice and score card configurations
 * and determines the best strategy to use that turn.
 */

package model;

import java.util.Iterator;

import model.ScoringCategories.*;

public class StrategyPicker {
	
	Dice dice = null;
	ScoreCard sc = null;
	static final int INITIAL_TURNS = 5;
	static final int ACCEPTABLE = 5;
	int turnsLeft = 0;
	int upperLeft = 0;
	int lowerLeft = 0;	

	public StrategyPicker(Dice dice, ScoreCard sc) {
		this.update(dice, sc);
	}
	
	public void update(Dice dice, ScoreCard sc) {
		this.dice = dice;
		this.sc = sc;
		
		Iterator<CategoryScore> iter = sc.iterator();
		Iterator<ScoreCategory> cats = sc.keyIterator();
		while (iter.hasNext()) {
			CategoryScore temp = iter.next();
			ScoreCategory cat = cats.next();
			if (!temp.isUsed()) {
				turnsLeft++;
				if (cat.isUpperSectionCategory()) {
					upperLeft++;
				}
				else {
					lowerLeft++;
				}
			}
		}
	}
	
	public YahtzeeStrategy strategize() { // best one active at the end...
		
//		return new UpperSectionerStrategy(); /* 117.9 */
//		return new FourAndUpStrategy(); /* 135.0 */
//		return new OfAKinderStrategy(); /* 135.1 */
		
//		/*** AVERAGE SCORE = 142.2 ***/
//		if (turnsLeft <= INITIAL_TURNS) {
//			return new OfAKinderStrategy();
//		}
//		
//		if (upperLeft < lowerLeft) {
//			return new FourAndUpStrategy();
//		}
//		else {
//			return new UpperSectionerStrategy();
//		}
		
		/*** AVERAGE SCORE = 145 ***/
		if (sc.getHighestScoringCategory(dice).getScore(dice) < ACCEPTABLE) {
			return new RandomStrategy();
		}
		else if (sc.isUpperSectionCategoryAvailable() && sc.isLowerSectionCategoryAvailable()
				&& sc.getUpperSectionTotal() < 63 
				&& sc.getHighestScoringUpperSectionCategory(dice).getScore(dice) 
					> sc.getHighestScoringLowerSectionCategory(dice).getScore(dice)) {
			return new UpperSectionerStrategy();
		}
		else {
			return new FourAndUpStrategy();
		}
	}

}
