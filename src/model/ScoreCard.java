/**
 * @author Daniel
 * @author Andrew
 * Created 3/31/2016
 * 
 * ScoreCard class models the Yahtzee score card, and provides a variety of access methods
 * Implements Iterator Pattern for easier iteration over categories and scores
 */

package model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import model.ScoringCategories.*;

public class ScoreCard implements Iterable<CategoryScore> {

	private Map<ScoreCategory, CategoryScore> scoreSheet; // map from category to score received
	
	public Map<ScoreCategory, CategoryScore> getScoreSheet() { return scoreSheet; }
	
	public ScoreCard(){
		// Initialize each category with score of 0
		scoreSheet = new HashMap<ScoreCategory, CategoryScore>();
		CategoryScore score = new CategoryScore();
		scoreSheet.put(new UpperSection(1),score);
		scoreSheet.put(new UpperSection(2),score);
		scoreSheet.put(new UpperSection(3),score);
		scoreSheet.put(new UpperSection(4),score);
		scoreSheet.put(new UpperSection(5),score);
		scoreSheet.put(new UpperSection(6),score);
		scoreSheet.put(new ThreeOfAKind(),score);
		scoreSheet.put(new FourOfAKind(),score);
		scoreSheet.put(new FullHouse(),score);
		scoreSheet.put(new SmallStraight(),score);
		scoreSheet.put(new LargeStraight(),score);
		scoreSheet.put(new Yahtzee(),score);
		scoreSheet.put(new Chance(),score);
	}
	
	public int getUpperSectionTotal() {
		int total = 0;
		for(Map.Entry<ScoreCategory, CategoryScore> entry : scoreSheet.entrySet()){
			
			// if score category is not already used and it's score is better than previously
			// seen score and it is an upper section category
			if(!entry.getValue().isUsed() && entry.getKey().isUpperSectionCategory()){ 
				total += entry.getValue().getScore();
			}
		}
		return total;
	}
	
	public ScoreCategory getHighestScoringCategory(Dice dice){
		int maxScore = 0;
		ScoreCategory maxSC = null;
		
		for(Map.Entry<ScoreCategory, CategoryScore> entry : scoreSheet.entrySet()){
			
			// if score category is not already used and it's score is better than previously
			// seen score
			if(!entry.getValue().isUsed() && (entry.getKey().getScore(dice) >= maxScore)){ 
				maxScore = entry.getKey().getScore(dice);
				maxSC = entry.getKey();
			}
		}			
		return maxSC;
	}
	
	public ScoreCategory getHighestScoringUpperSectionCategory(Dice dice){
		
		ScoreCategory maxSC = null;
		int maxScore = 0;
		
		for(Map.Entry<ScoreCategory, CategoryScore> entry : scoreSheet.entrySet()){
			
			// if score category is not already used and it's score is better than previously
			// seen score and it is an upper section category
			if(!entry.getValue().isUsed() && (entry.getKey().getScore(dice) >= maxScore) &&
					entry.getKey().isUpperSectionCategory()){ 
				maxSC = entry.getKey();
				maxScore = entry.getKey().getScore(dice);
			}
		}
		return maxSC;
	}
	
	public ScoreCategory getHighestScoringLowerSectionCategory(Dice dice){
			
			ScoreCategory maxSC = null;
			int maxScore = 0;
			
			for(Map.Entry<ScoreCategory, CategoryScore> entry : scoreSheet.entrySet()){
				
				// if score category is not already used and it's score is better than previously
				// seen score and it is an upper section category
				if(!entry.getValue().isUsed() && (entry.getKey().getScore(dice) >= maxScore) &&
						!entry.getKey().isUpperSectionCategory()){ 
					maxSC = entry.getKey();
					maxScore = entry.getKey().getScore(dice);
				}
			}
			return maxSC;
		}
		
	public void useCategory(ScoreCategory category, Dice dice){
		CategoryScore score = new CategoryScore(category.getScore(dice), true);
		
		scoreSheet.put(category, score);
		System.out.println(category + "\t" + score);
	}
	
	public boolean isCategoryAvailable(ScoreCategory category){
		return !scoreSheet.get(category).isUsed();
	}
		
	public boolean isUpperSectionCategoryAvailable(){
		for (Map.Entry<ScoreCategory, CategoryScore> entry : scoreSheet.entrySet()) {
			
			if(entry.getKey().isUpperSectionCategory()){
				if(!entry.getValue().isUsed())
					return true;
			}
		}		
			
		return false;
	}
		
	public boolean isLowerSectionCategoryAvailable(){
	
		for (Map.Entry<ScoreCategory, CategoryScore> entry : scoreSheet.entrySet()) {
				
			if(!entry.getKey().isUpperSectionCategory()){
				if(!entry.getValue().isUsed())
					return true;
			}
		}		
		return false;
	}
		
	public CategoryScore get(ScoreCategory sc){
		return scoreSheet.get(sc);
	}
		
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		for (Map.Entry<ScoreCategory, CategoryScore> entry : scoreSheet.entrySet()) {
			sb.append(entry.getKey() + " " + entry.getValue() + "\n");
		}
		return sb.toString();
	}
	
	@Override
	public Iterator<CategoryScore> iterator() {
		return scoreSheet.values().iterator();
	}
		
	public Iterator<ScoreCategory> keyIterator() {
		return scoreSheet.keySet().iterator();
	}

}
