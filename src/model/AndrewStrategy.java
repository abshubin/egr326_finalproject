/**
 * @author Andrew
 * Created 4/10/2016
 * 
 * AndrewStrategy is a special strategy designed by Andrew
 */

package model;

import java.util.Iterator;

import model.ScoringCategories.*;

public class AndrewStrategy implements YahtzeeStrategy {

	@Override
	public void reRollDice(Dice dice, ScoreCard sc) {
		if (DiceHelper.containsYahtzee(dice)) {
			return;
		}
		else if ((DiceHelper.containsFourOfAKind(dice) || DiceHelper.containsFourOfAKind(dice))
				&& (!sc.get(new FourOfAKind()).isUsed() || !sc.get(new ThreeOfAKind()).isUsed())) {
			DiceHelper.reRollUnmatchingDie(dice);
		}
		else if ((DiceHelper.containsLargeStraight(dice) || DiceHelper.containsLargeStraight(dice))
				&& (!sc.get(new LargeStraight()).isUsed() || !sc.get(new SmallStraight()).isUsed())) {
			DiceHelper.reRollNonConsecutive(dice, 5);
		}
		else if (DiceHelper.containsFullHouse(dice)) {
			return;
		}
		else if (DiceHelper.containsThreeOfAKind(dice) || DiceHelper.containsPair(dice)) {
			DiceHelper.reRollUnmatchingDie(dice);
		}
		else if (DiceHelper.containsAlmostSmallStraight(dice) && !sc.get(new SmallStraight()).isUsed()) {
			DiceHelper.reRollNonConsecutive(dice, 5);
		}
		else {
			dice.rollAll();
		}
		
		System.out.println(dice);	
	}

	@Override
	public void selectCategory(Dice dice, ScoreCard sc) {
		
		ScoreCategory selectedCategory = null;
		ScoreCategory[] cats = new ScoreCategory[13];
		Iterator<ScoreCategory> iter = sc.keyIterator();
		int count = 0;
		while (iter.hasNext()) {
			cats[count] = iter.next();
			count++;
		}
		/*
		 *  0. Aces
		 *  1. Twos
		 *  2. Threes
		 *  3. Fours
		 *  4. Fives
		 *  5. Sixes
		 *  6. Three of a kind
		 *  7. Four of a kind
		 *  8. Full house
		 *  9. Small straight
		 * 10. Large straight
		 * 11. Yahtzee
		 * 12. Chance
		 */
		
		if (!sc.get(cats[11]).isUsed() && cats[11].isValidCategory(dice)) {
			selectedCategory = cats[11];
		}
		else if (DiceHelper.containsFourOfAKind(dice)) {
			for (int i = 0 ; i < 6 ; i++) {
				if (cats[i].isUpperSectionCategory()) {
					UpperSection temp = (UpperSection) cats[i];
					if (temp.getFrequency(dice) >= 4) {
						if (!sc.get(temp).isUsed()) {
							if (temp.getValue() > 3) {
								selectedCategory = temp;
							}
							else if (!sc.get(cats[7]).isUsed()) {
								selectedCategory = cats[7];
							}
							else if (!sc.get(cats[6]).isUsed()) {
								selectedCategory = cats[6];
							}
							else {
								selectedCategory = highestAcceptable(dice, sc, cats);
							}
						}
						else {
							selectedCategory = findUpperThree(dice, sc, cats);
							if (selectedCategory == null) {
								selectedCategory = highestAcceptable(dice, sc, cats);
							}
						}
					}
				}
			}
		}
		else if (DiceHelper.containsLargeStraight(dice)
				&& (!sc.get(cats[10]).isUsed()
					|| !sc.get(cats[9]).isUsed())) {
			if (!sc.get(cats[10]).isUsed()) {
				selectedCategory = cats[10];
			}
			else {
				selectedCategory = cats[9];
			}
		}
		else if (DiceHelper.containsSmallStraight(dice)
				&& !sc.get(cats[9]).isUsed()) {
			selectedCategory = cats[9];
		}
		else if (DiceHelper.containsFullHouse(dice)) {
			if (sc.get(cats[8]).isUsed()) {
				selectedCategory = cats[8];
			}
			else {
				selectedCategory = findUpperThree(dice, sc, cats);
				if (selectedCategory == null) {
					selectedCategory = highestAcceptable(dice, sc, cats);
				}
			}
		}
		else if (DiceHelper.containsThreeOfAKind(dice)) {
			if (!sc.get(cats[6]).isUsed()) {
				selectedCategory = cats[6];
			}
			else {
				selectedCategory = findUpperThree(dice, sc, cats);
				if (selectedCategory == null) {
					selectedCategory = highestAcceptable(dice, sc, cats);
				}
			}
		}
		else {
			selectedCategory = sc.getHighestScoringCategory(dice);
		}
		
		System.out.println("Selecting category: " + selectedCategory);
		System.out.println("Score: " + selectedCategory.getScore(dice));
		
		sc.useCategory(selectedCategory,dice);	
	}
	
	private ScoreCategory findUpperThree(Dice dice, ScoreCard sc, ScoreCategory[] cats) {
		for (int i = 0 ; i < 6 ; i++) {
			UpperSection temp = (UpperSection) cats[i];
			if (temp.getFrequency(dice) >= 3
					&& !sc.get(temp).isUsed()) {
				return temp;
			}
		}
		return null;
	}
	
	private ScoreCategory lowestAvailable(ScoreCard sc, ScoreCategory[] cats) {
		ScoreCategory exceedsThree = null;
		for (int i = 0 ; i < 13 ; i++) {
			if (!sc.get(cats[i]).isUsed()) {
				return cats[i];
			}
		}
		return exceedsThree;
	}
	
	private ScoreCategory highestAcceptable(Dice dice, ScoreCard sc, ScoreCategory[] cats) {
		int lowestAcceptableScore = 10;
		ScoreCategory retval = null;
		retval = sc.getHighestScoringCategory(dice);
		if (retval.getScore(dice) < lowestAcceptableScore) {
			retval = lowestAvailable(sc, cats);
		}
		return retval;
	}
	
	@Override
	public String toString(){
		return "ANDREW'S STRATEGY";
	}

}
