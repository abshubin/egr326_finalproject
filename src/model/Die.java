/**
 * @author Daniel
 * Created 3/31/2016
 * 
 * Simple Die class to return random die roll
 */

package model;

import java.util.Random;

public class Die implements Comparable<Die> {
	private int lastRoll = 1;
	private int index = 0;
	
	public Die(int i){
		index = i;
	}
	
	public int roll(){
		Random rand = new Random();
		lastRoll = rand.nextInt(6) + 1;
		return lastRoll;
	}
	
	public int getLastRoll() { return lastRoll; }
	public int getIndex() { return index; }
	
	@Override
	public int hashCode(){
		return lastRoll;
	}
	
	@Override
	public boolean equals(Object o){
		if (o.getClass() == this.getClass() && o != null){
			Die d = (Die) o;
			return d.getLastRoll() == lastRoll;
		}
		return false;
		
	}

	@Override
	public int compareTo(Die d) {
		return (this.getLastRoll() < d.getLastRoll()) ? -1 : 1;
	}
}
