/**
 * @author Daniel
 * Created 4/1/2016
 * 
 * LargeStraight class implements the ScoreCategory for the large straight scoring category
 */

package model.ScoringCategories;

import model.Dice;
import model.DiceHelper;

public class LargeStraight implements ScoreCategory {
	
	@Override
	public int getScore(Dice dice) {
		
		if (isValidCategory(dice)) {
			return 40;
		}
		else
			return 0;
	}

	@Override
	public boolean isValidCategory(Dice dice) {
		return DiceHelper.containsLargeStraight(dice);
	}

	@Override
	public boolean isUpperSectionCategory() {
		return false;
	}

	@Override
	public int hashCode() {
		return 11;
	}
	
	@Override
	public boolean equals(Object o){
		if(o.getClass() == getClass() && o != null){
			ScoreCategory sc = (ScoreCategory)o;
			return hashCode() == sc.hashCode();
		}
		return false;			
	}
	
	@Override
	public String toString(){
		return "LARGE STRAIGHT";
	}
	
	/*    FOR TESTING
	public static void main(String[] args){
		Dice dice = new Dice(5);
		System.out.println(dice);
		ScoreCategory lrgStrt = new LargeStraight();
		dice.rollAll();
		while(!lrgStrt.isValidCategory(dice)){
			dice.rollAll();
			System.out.println(dice);
		}
		System.out.println(dice);
		System.out.println(lrgStrt.getScore(dice));
	}
	*/
}
