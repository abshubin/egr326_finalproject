/**
 * @author Daniel
 * Created 4/1/2016
 * 
 * SmallStraight class implements the ScoreCategory for the small straight scoring category
 */

package model.ScoringCategories;

import model.Dice;
import model.DiceHelper;

public class SmallStraight implements ScoreCategory {
	
	@Override
	public int getScore(Dice dice) {
		
		if (isValidCategory(dice)) {
			return 30;
		}
		else
			return 0;
	}

	@Override
	public boolean isValidCategory(Dice dice) {
		return DiceHelper.containsSmallStraight(dice);
	}

	@Override
	public boolean isUpperSectionCategory() {
		return false;
	}
	
	@Override
	public int hashCode() {
		return 10;
	}
	
	@Override
	public boolean equals(Object o){
		if(o.getClass() == getClass() && o != null){
			ScoreCategory sc = (ScoreCategory)o;
			return hashCode() == sc.hashCode();
		}
		return false;			
	}
	
	@Override
	public String toString(){
		return "SMALL STRAIGHT";
	}

	
	/*    FOR TESTING
	public static void main(String[] args){
		Dice dice = new Dice(5);
		System.out.println(dice);
		ScoreCategory category = new SmallStraight();
		dice.rollAll();
		while(!category.isValidCategory(dice)){
			dice.rollAll();
			System.out.println(dice);
		}
		System.out.println(dice);
		System.out.println(category.getScore(dice));
	}
	*/
}
