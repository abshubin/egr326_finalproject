/**
 * @author Daniel
 * Created 4/1/2016
 * 
 * ScoreCategory interface provides an interface for all scoring categories
 */

package model.ScoringCategories;

import model.Dice;

public interface ScoreCategory {
	int getScore(Dice dice);
	boolean isValidCategory(Dice dice);
	boolean isUpperSectionCategory();
}
