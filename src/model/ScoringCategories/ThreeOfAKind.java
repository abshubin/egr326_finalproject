/**
 * @author Daniel
 * Created 4/1/2016
 * 
 * ThreeOfAKind class implements the ScoreCategory for the three-of-a-kind scoring category
 */

package model.ScoringCategories;

import model.Dice;
import model.DiceHelper;

public class ThreeOfAKind implements ScoreCategory {
	
	@Override
	public int getScore(Dice dice) {
		
		if(isValidCategory(dice))
			return DiceHelper.sum(dice);
		else
			return 0;
	}

	@Override
	public boolean isValidCategory(Dice dice) {
		return DiceHelper.containsThreeOfAKind(dice);
	}

	@Override
	public boolean isUpperSectionCategory() {
		return false;
	}

	@Override
	public int hashCode() {
		return 7;
	}

	@Override
	public boolean equals(Object o){
		if(o.getClass() == getClass() && o != null){
			ScoreCategory sc = (ScoreCategory)o;
			return hashCode() == sc.hashCode();
		}
		return false;			
	}
	
	@Override
	public String toString(){
		return "THREE-OF-A-KIND";
	}
	
	/*    FOR TESTING
	public static void main(String[] args){
		Dice dice = new Dice(5);
		System.out.println(dice);
		ScoreCategory category = new ThreeOfAKind();
		dice.rollAll();
		while(!category.isValidCategory(dice)){
			dice.rollAll();
			System.out.println(dice);
		}
		System.out.println(dice);
		System.out.println(category.getScore(dice));
	}
	*/
}
