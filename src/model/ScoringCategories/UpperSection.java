/**
 * @author Daniel
 * Created 4/3/2016
 * 
 * UpperSection class implements the ScoreCategory for the upper section scoring categories
 */

package model.ScoringCategories;

import model.Dice;
import model.Die;

public class UpperSection implements ScoreCategory {
	
	private int value;
	
	public UpperSection(int value){
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public int getFrequency(Dice dice) {
		int freq = 0;
		for (int i = 0 ; i < 5 ; i++) {
			if (dice.getDice().get(i).getLastRoll() == value) {
				freq++;
			}
		}
		return freq;
	}
	
	@Override
	public int getScore(Dice dice) {
		return this.getFrequency(dice) * value;
	}

	@Override
	public boolean isValidCategory(Dice dice) {		
		return true;
	}
	@Override
	public boolean isUpperSectionCategory() {
		return true;
	}

	@Override
	public int hashCode() {
		return value;
	}

	@Override
	public boolean equals(Object o){
		if(o.getClass() == getClass() && o != null){
			ScoreCategory sc = (ScoreCategory)o;
			return hashCode() == sc.hashCode();
		}
		return false;			
	}
	
	@Override
	public String toString(){
		switch(value){
		case 1: return "ACES";
		case 2: return "TWOS";
		case 3: return "THREES";
		case 4: return "FOURS";
		case 5: return "FIVES";
		case 6: return "SIXES";
		default: return "FAIL";
		}
	}

}
