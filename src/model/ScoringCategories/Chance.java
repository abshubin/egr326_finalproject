/**
 * @author Daniel
 * Created 4/1/2016
 * 
 * Chance class implements the ScoreCategory for the "chance" scoring category
 */

package model.ScoringCategories;

import model.Dice;
import model.DiceHelper;

public class Chance implements ScoreCategory {
	
	@Override
	public int getScore(Dice dice) {
		return DiceHelper.sum(dice);
	}

	@Override
	public boolean isValidCategory(Dice dice) {
		return true;
	}

	@Override
	public boolean isUpperSectionCategory() {
		return false;
	}

	@Override
	public int hashCode() {
		return 13;
	}
	
	@Override
	public boolean equals(Object o){
		if(o.getClass() == getClass() && o != null){
			ScoreCategory sc = (ScoreCategory)o;
			return hashCode() == sc.hashCode();
		}
		return false;			
	}
	
	@Override
	public String toString(){
		return "CHANCE";
	}

	/*    FOR TESTING
	public static void main(String[] args){
		Dice dice = new Dice(5);
		System.out.println(dice);
		ScoreCategory category = new Chance();
		dice.rollAll();
		while(!category.isValidCategory(dice)){
			dice.rollAll();
			System.out.println(dice);
		}
		System.out.println(dice);
		System.out.println(category.getScore(dice));
	}
	*/
}
