/**
 * @author Daniel
 * Created 3/31/2016
 * 
 * UpperSectionerStrategy class implements YahtzeeStrategy to focus on scoring in the upper sections
 */

package model;

import model.ScoringCategories.ScoreCategory;

public class UpperSectionerStrategy  implements YahtzeeStrategy {

	@Override
	public void reRollDice(Dice dice, ScoreCard sc) {
		
		DiceHelper.reRollUnmatchingDie(dice);
		
		System.out.println("UPPER SECTIONER STRATEGY SELECTED");
		System.out.println("Re-rolling unmatching dice. " + dice);
	}

	@Override
	public void selectCategory(Dice dice, ScoreCard sc) {
		ScoreCategory selectedCategory = null;
		
		if(sc.isUpperSectionCategoryAvailable())
			selectedCategory = sc.getHighestScoringUpperSectionCategory(dice);
		else
			selectedCategory = sc.getHighestScoringCategory(dice);
		
		System.out.println("UPPER SECTIONER STRATEGY SELECTED");
		System.out.println("Selecting highest scoring category: " + selectedCategory);
		System.out.println("Score: " + selectedCategory.getScore(dice));
		
		sc.useCategory(selectedCategory,dice);
	}

}