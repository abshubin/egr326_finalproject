/**
 * @author Daniel
 * Created 3/31/2016
 * 
 * RandomStrategy class implements YahtzeeStrategy to roll and score randomly
 */

package model;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import model.ScoringCategories.ScoreCategory;
import model.ScoringCategories.*;

public class RandomStrategy implements YahtzeeStrategy {

	@Override
	public void reRollDice(Dice dice, ScoreCard sc) {
		
		Set<Integer> indices = new HashSet<>();
		
		Random rand = new Random();
		int randomNumOfDice = rand.nextInt(5) + 1;
		for (int i = 0; i < randomNumOfDice; i++){
			indices.add(rand.nextInt(5));
		}
		
		dice.rollSpecifiedDie(indices);
		
		System.out.println("RANDOM STRATEGY SELECTED");
		System.out.println("Re-rolling random dice. " + dice);
		
	}

	@Override
	public void selectCategory(Dice dice, ScoreCard sc) {
		ScoreCategory selected = null;
		
		Random rand = new Random();
		int num = rand.nextInt(13);
		
		while (selected == null) {
			if (num == 0) {
				if (!sc.isCategoryAvailable(new UpperSection(1)))
					num++;
				else
					selected = new UpperSection(1);
			} else if (num == 1) {
				if (!sc.isCategoryAvailable(new UpperSection(2)))
					num++;
				else
					selected = new UpperSection(2);
			} else if (num == 2) {
				if (!sc.isCategoryAvailable(new UpperSection(3)))
					num++;
				else
					selected = new UpperSection(3);
			} else if (num == 3) {
				if (!sc.isCategoryAvailable(new UpperSection(4)))
					num++;
				else
					selected = new UpperSection(4);
			} else if (num == 4) {
				if (!sc.isCategoryAvailable(new UpperSection(5)))
					num++;
				else
					selected = new UpperSection(5);
			} else if (num == 5) {
				if (!sc.isCategoryAvailable(new UpperSection(6)))
					num++;
				else
					selected = new UpperSection(6);
			} else if (num == 6) {
				if (!sc.isCategoryAvailable(new ThreeOfAKind()))
					num++;
				else
					selected = new ThreeOfAKind();
			} else if (num == 7) {
				if (!sc.isCategoryAvailable(new FourOfAKind()))
					num++;
				else
					selected = new FourOfAKind();
			} else if (num == 8) {
				if (!sc.isCategoryAvailable(new FullHouse()))
					num++;
				else
					selected = new FullHouse();
			} else if (num == 9) {
				if (!sc.isCategoryAvailable(new SmallStraight()))
					num++;
				else
					selected = new SmallStraight();
			} else if (num == 10) {
				if (!sc.isCategoryAvailable(new LargeStraight()))
					num++;
				else
					selected = new LargeStraight();
			} else if (num == 11) {
				if (!sc.isCategoryAvailable(new Yahtzee()))
					num++;
				else
					selected = new Yahtzee();
			} else if (num == 12) {
				if (!sc.isCategoryAvailable(new Chance()))
					num = 0;
				else
					selected = new Chance();
			}
		}
		
		sc.useCategory(selected, dice);
		System.out.println("RANDOM STRATEGY SELECTED");
		System.out.println("Selecting a random category.");
		
	}
	/* FOR TESTING
	public static void main(String[] args){
		Dice dice = new Dice(5);
		YahtzeeStrategy strat = new RandomStrategy();
		ScoreCard sc = new ScoreCard();
		
		dice.rollAll();
		System.out.println(dice);
		for(int i = 0; i < 13; i++){
			dice.rollAll();
			strat.selectCategory(dice, sc);
		}
	}
	*/
}
