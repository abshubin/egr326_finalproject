/**
 * @author Daniel
 * Created 3/31/2016
 * 
 * Player class contains strategy and keeps track of game state
 */

package model;

import java.util.Map;

import model.Game.GameState;
import model.ScoringCategories.ScoreCategory;

public class Player {

	private YahtzeeStrategy strategy;
	private GameState state;
	private String name;
	private ScoreCard scoreCard = new ScoreCard();
	private boolean human;
	
	public Player(String name, boolean human){
		this.name = name;
		state = GameState.END_TURN;
		this.human = human;
	}
	
	// ACCESSORS
	public GameState getState() { return state; }
	public String getName() { return name; }
	public ScoreCard getScoreCard() { return scoreCard; }
	public boolean isHuman() { return human; }
	
	// MUTATORS
	public void setStrategy(YahtzeeStrategy strat) { strategy = strat; }
	public void setState(GameState state) { this.state = state; }
	
	
	public void executeStrategy(Dice dice){
		switch(state){
		case FIRST_ROLL:
			strategy.reRollDice(dice, scoreCard);
			setState(GameState.SECOND_ROLL);
			break;
		case SECOND_ROLL:
			strategy.reRollDice(dice, scoreCard);
			setState(GameState.THIRD_ROLL);
			break;
		case THIRD_ROLL:
			strategy.selectCategory(dice, scoreCard);
			setState(GameState.END_TURN);
			break;
		case END_TURN:
			break;
		default:
			break;
		}
	}
	
	public int getCurrentScore(){
		int upperScore = 0;
		int lowerScore = 0;
		int bonus = 0;
		for(Map.Entry<ScoreCategory, CategoryScore> entry : scoreCard.getScoreSheet().entrySet()){
			if (entry.getKey().isUpperSectionCategory())
				upperScore += entry.getValue().getScore();
			else
				lowerScore += entry.getValue().getScore();
		}
		if (upperScore >= 63)
			bonus = 35;
		
		return upperScore + lowerScore + bonus;
	}
	
	@Override
	public String toString(){
		return name + " - Score: " + getCurrentScore();
	}
}
