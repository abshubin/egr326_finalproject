/**
 * @author Daniel
 * Created 4/4/2016
 * 
 * CategoryScore class is used to keep track of score and usage stats for a category (stored with associated category)
 */

package model;

public class CategoryScore {
	private boolean used;
	private int score;
	
	public CategoryScore(){
		used = false;
		score = 0;
	}
	
	public CategoryScore(int score, boolean used){
		this.score = score;
		this.used = used;
	}
	
	public int getScore() { return score; }
	public boolean isUsed() { return used; }
	
	public void setScore(int score) { this.score = score; }
	public void setUsed(boolean used) { this.used = used; }
	
	@Override
	public String toString(){
		return (used ? "**USED**" : "") + "   " + score;
	}
}
