/**
 * @author Daniel
 * Created 4/4/2016
 * 
 * DanielStrategy is a special strategy designed by Daniel
 */

package model;

import model.ScoringCategories.*;

public class DanielStrategy implements YahtzeeStrategy {

	@Override
	public void reRollDice(Dice dice, ScoreCard sc) {
		// First check for Large Straight
		if( DiceHelper.containsLargeStraight(dice) ){
			if(!sc.get(new LargeStraight()).isUsed() )
				return;
			else if (!sc.get(new SmallStraight()).isUsed())
				return;
			else // I've used both small and large straight, 
				dice.rollAll();				
		}
		// Not a Large Straight
		// Check for Small Straight
		else if	(DiceHelper.containsSmallStraight(dice) ){
			if (!sc.get(new SmallStraight()).isUsed() )
				return;
			else if (!sc.get(new LargeStraight()).isUsed()) // try to get the large straight
				DiceHelper.reRollNonConsecutive(dice, 4);
			else
				dice.rollAll();
		}
		// Next make sure we don't screw up a 3-of-a-kind
		else if (DiceHelper.containsThreeOfAKind(dice))
			DiceHelper.reRollUnmatchingDie(dice);
		else if (DiceHelper.containsPair(dice))
			DiceHelper.reRollUnmatchingDie(dice);
		// Now check for almost straights
//		else if (DiceHelper.containsAlmostSmallStraight(dice))
//			DiceHelper.reRollNonConsecutive(dice, 3);
		// Everything else will benefit from more matching die
		else
			DiceHelper.reRollUnmatchingDie(dice);
		
		System.out.println(dice);
	}

	@Override
	public void selectCategory(Dice dice, ScoreCard sc) {
		
		ScoreCategory selectedCategory = null;
		
		if(sc.isLowerSectionCategoryAvailable() && (sc.getHighestScoringLowerSectionCategory(dice).getScore(dice) > 0))
			selectedCategory = sc.getHighestScoringLowerSectionCategory(dice);
		else
			selectedCategory = sc.getHighestScoringCategory(dice);
		
		System.out.println("Selecting highest scoring category: " + selectedCategory);
		System.out.println("Score: " + selectedCategory.getScore(dice));
		
		sc.useCategory(selectedCategory,dice);
		
	}
	
	@Override
	public String toString(){
		return "DANIEL'S STRATEGY";
	}

}
