/**
 * @author Andrew
 * @author Daniel
 * Created 4/4/2016
 * 
 * DiceHelper class is where we moved all the algorithms for determining whether a dice set was valid for a score category
 */

package model;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeSet;

public class DiceHelper {
	
	public static boolean containsPair(Dice dice){
		return containsDuplicates(dice, 2);
	}
	
	public static boolean containsThreeOfAKind(Dice dice){
		return containsDuplicates(dice, 3);
	}
	
	public static boolean containsFourOfAKind(Dice dice){
		return containsDuplicates(dice, 4);
	}
	
	public static boolean containsYahtzee(Dice dice){
		return containsDuplicates(dice, 5);
	}
	
	public static boolean containsSmallStraight(Dice dice){
		return containsConsecutiveNumbers(dice, 4);
	}
	
	public static boolean containsLargeStraight(Dice dice){
		return containsConsecutiveNumbers(dice, 5);
	}
	
	public static boolean containsFullHouse(Dice dice){
		
		boolean three = false; // is there a three of a kind?
		boolean two = false; // is there a two of a kind?
		
		int[] count = new int[7]; // index 0 is not used
		for(Die d : dice.getDice()){ // record the amount of each number rolled in count[numberRolled]
			count[d.getLastRoll()]++;
		}
		for (int i = 1 ; i < 7 ; i++) { // check for full-house conditions
			if (count[i] == 2) {
				two = true;
			}
			else if (count[i] == 3) {
				three = true;
			}
		}
		if (three && two) { // if there is a two-of-a-kind and a three-of-a-kind
			return true; 
		}
		return false;		
	}
	
	public static boolean containsAlmostSmallStraight(Dice dice){
		return containsConsecutiveNumbers(dice, 3);
	}
	
	public static void reRollNonConsecutive(Dice dice, int min){
		
		Set<Integer> indices = new HashSet<Integer>();
		Stack<Integer> temp = new Stack<Integer>();
		Collections.sort(dice.getDice());
		
		temp.push(dice.getDice().get(0).getLastRoll());
		for(int i = 1; i < dice.getDice().size(); i++){
			if(dice.getDice().get(i-1).getLastRoll() == dice.getDice().get(i).getLastRoll() ){
				indices.add(i);
			} 
			else if (dice.getDice().get(i-1).getLastRoll() == (dice.getDice().get(i).getLastRoll() - 1) ){
				temp.push(i);
			}
			else {
				if (temp.size() != min){
					temp.pop();
					temp.push(i);
				}
				else {
					indices.add(i);
				}
			}
		}
		
		dice.rollSpecifiedDie(indices);
	}
	
	public static void reRollUnmatchingDie(Dice dice){
		Set<Integer> indices = new HashSet<>();
		Map<Die, Integer> repeatedValues = new HashMap<>();
		
		// need to get the indices of the dice with values that occur only once
		for(Die d : dice.getDice()){
			
			if(repeatedValues.containsKey(d)){ // A die is considered equal if the lastRoll is equal
				int numOccurences = repeatedValues.get(d);
				numOccurences++;
				repeatedValues.put(d, numOccurences);
			} else{
				repeatedValues.put(d, 1);
			}
		}
		
		for (Map.Entry<Die,Integer> entry : repeatedValues.entrySet()){
			if(entry.getValue() < 2){
				indices.add(entry.getKey().getIndex());
			}
		}
		
		dice.rollSpecifiedDie(indices);
	}
	
	public static int sum(Dice dice){
		int sum = 0;
		for(Die d : dice.getDice()){
			sum += d.getLastRoll();
		}
		return sum;
	}
	
	private static boolean containsDuplicates(Dice dice, int numDuplicates){
		if(numDuplicates > dice.getDice().size())
			return false;
		
		int[] count = new int[7]; // index 0 is not used
		for(Die d : dice.getDice()){ // record the amount of each number rolled in count[numberRolled]
			count[d.getLastRoll()]++;
		}
		for (int i = 1 ; i < 7 ; i++) { // check for four-of-a-kind conditions
			if (count[i] >= numDuplicates) {
				return true;
			}
		}
		return false;
	}
	
	private static boolean containsConsecutiveNumbers(Dice dice, int numConsec){
		
		// create a sorted set array of the dice values
		SortedSet<Integer> values = new TreeSet<Integer>();
		for(Die d : dice.getDice()){
			values.add(d.getLastRoll());
		}
		if(values.size() < numConsec)
			return false;
		
		Integer[] set = values.toArray(new Integer[values.size()]);
		int count = 1;
		for(int i = 1; i < values.size(); i++){
			if( set[i-1] == (set[i] - 1) )
				count++;
			else
				count = 1;
			if(count == numConsec)
				return true;
		}
		return false;
	}
	
	/* FOR TESTING
	public static void main(String[] args){
		Dice dice = new Dice(5);
		dice.rollAll();
		System.out.println(dice);
		while (!DiceHelper.containsAlmostSmallStraight(dice)){
			dice.rollAll();
			System.out.println(dice);
		}
		DiceHelper.reRollNonConsecutive(dice, 3);
		System.out.println(dice);
	}
	*/
}
