/**
 * @author Daniel
 * Created 3/31/2016
 * 
 * Dice class for rolling all dice and specific dice
 */

package model;

import java.util.ArrayList;
import java.util.Set;

public class Dice {
	private ArrayList<Die> dice;
	
	public Dice(int numDie){
		if (numDie <= 0)
			return;
		
		dice = new ArrayList<Die>();
		for (int i = 0; i < numDie; i++)
			dice.add(new Die(i));
	}
	
	public ArrayList<Die> getDice() { return dice; }
	
	public void rollAll(){
		for (Die d : dice)
			d.roll();
	}
	
	public void rollSpecifiedDie(Set<Integer> indices){
		
		for(Integer i : indices){
			if (i >= 0 && i <= (dice.size() - 1) ) 
				dice.get(i).roll();
		}
	}
	
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		for(Die d : dice){
			sb.append(d.getLastRoll() + " ");
		}
		return sb.toString();
	}
}
