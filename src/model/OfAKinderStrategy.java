/**
 * @author Daniel
 * Created 3/31/2016
 * 
 * OfAKinderStrategy class implements YahtzeeStrategy to focus on getting as many dice as possible of a kind
 */

package model;

public class OfAKinderStrategy implements YahtzeeStrategy {

	@Override
	public void reRollDice(Dice dice, ScoreCard sc) {
		DiceHelper.reRollUnmatchingDie(dice);
		
		System.out.println("OF-A-KINDER STRATEGY SELECTED");
		System.out.println("Re-rolling unmatching dice. " + dice);
		
	}

	@Override
	public void selectCategory(Dice dice, ScoreCard sc) {
		
		System.out.println("OF-A-KINDER STRATEGY SELECTED");
		System.out.println("Selecting highest scoring category: " + sc.getHighestScoringCategory(dice));
		System.out.println("Score: " + sc.getHighestScoringCategory(dice).getScore(dice));
		
		sc.useCategory(sc.getHighestScoringCategory(dice), dice);		
		
	}
	
	/* FOR TESTING
	public static void main(String[] args){
		final int NUM_ROLLS = 5;
		Dice dice = new Dice(5);
		YahtzeeStrategy strat = new OfAKinderStrategy();
		
		dice.rollAll();
		System.out.println(dice);
		for(int i = 0; i < NUM_ROLLS; i++){
			strat.reRollDice(dice, new ScoreCard());
			System.out.println(dice);
		}
	}
	*/
}