/**
 * @author Daniel
 * Created 3/31/2016
 * 
 * Strategy interface for implementing the strategy design pattern
 */

package model;

public interface YahtzeeStrategy {
	void reRollDice(Dice dice, ScoreCard sc);
	void selectCategory(Dice dice, ScoreCard sc);
}
